﻿using System.Collections.Generic;
using UnityEngine;
public class Inventory: ScriptableObject
{
    private List<Item> items;

    public Inventory()
    {
        items = new List<Item>();
    }

    public List<Item> GetItemList()
    {
        return items;
    }

    public void Add(Item item)
    {
        Item itemSearch = items.Find(e => e.name == item.name);
        if (itemSearch != null)
        {
            //Item already exists in inventory.
            itemSearch.amount++;
        }
        else
        {
            Item newItem = item;
            items.Add(newItem);
        }
    }

    public bool ItemExists(Item item)
    {
        Item itemSearch = items.Find(e => e.name == item.name);
        return itemSearch == null;
    }

    public void Remove(Item item)
    {
        items.Remove(item);
    }
}
