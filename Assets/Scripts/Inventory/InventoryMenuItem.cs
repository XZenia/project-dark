﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class InventoryMenuItem : MonoBehaviour, IBeginDragHandler
{
    public Image itemSprite;
    public TextMeshProUGUI itemAmountText;

    private Item itemInformation;

    private RectTransform rectTransform;

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public void setItemMenu(Item _itemInformation)
    {
        itemInformation = _itemInformation;

        itemSprite.sprite = itemInformation.icon;
        itemAmountText.text = "x" + itemInformation.amount;

        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("Item dragging");
        rectTransform.anchoredPosition += eventData.delta;
    }
}
