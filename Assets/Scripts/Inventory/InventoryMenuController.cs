﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryMenuController : MonoBehaviour
{
    [SerializeField] private GameObject buttonTemplate;
    [SerializeField] private GameObject inventoryContent;

    private Inventory inventory;

    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;
        RefreshInventoryItems();
    }

    public void RefreshInventoryItems()
    {
        foreach(Transform child in inventoryContent.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (Item item in inventory.GetItemList())
        {
            GameObject inventoryButton = Instantiate(buttonTemplate) as GameObject;
            inventoryButton.GetComponent<InventoryMenuItem>().setItemMenu(item);
            inventoryButton.transform.SetParent(inventoryContent.transform, false);
            inventoryButton.SetActive(true);
        }

    }
}
