﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerScript : MonoBehaviour
{
    public int speed;

    public Vector2 targetPosition;
    private Vector2 previousTargetPosition;

    private bool isWalking = false;
    private Animator playerAnimator;

    private Inventory inventory;
    [SerializeField] private InventoryMenuController inventoryMenuController; 

    void Start()
    {
        playerAnimator = gameObject.GetComponent<Animator>();
        previousTargetPosition = gameObject.transform.position;

        inventory = new Inventory();
        inventoryMenuController.SetInventory(inventory);
        
        gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
    }

    public void AddItemInInventory(Item item)
    {
        inventory.Add(item);
        inventoryMenuController.RefreshInventoryItems();
    }

    void Update()
    {
        if (isWalking)
        {
            targetPosition.y = gameObject.transform.position.y;

            gameObject.transform.position = Vector2.MoveTowards(transform.position, targetPosition, Time.deltaTime * speed);

            FlipSprite();

            previousTargetPosition = gameObject.transform.position;

            if (transform.position.x == targetPosition.x)
            {
                playerAnimator.SetBool("IsMoving", false);
                gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                isWalking = false;
            }
            else
            {
                playerAnimator.SetBool("IsMoving", true);
            }
        }
    }

    private void FlipSprite()
    {
        if (previousTargetPosition.x > targetPosition.x)
        {
            //Facing right
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        if (previousTargetPosition.x < targetPosition.x)
        {
            //Facing left
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    public void MoveTowards(Vector2 coordinates)
    {
        targetPosition = coordinates;
        isWalking = true;
    }
}
