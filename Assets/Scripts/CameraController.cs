﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.125f;
    public float offset;

    void LateUpdate()
    {
        Vector3 position = transform.position;

        position.x = target.position.x;
        transform.position = position;
    }
}
