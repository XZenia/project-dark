﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Items")]
public class Item : ScriptableObject
{
    public string name = "Item";
    public Sprite icon = null;
    public string description = "This is an item.";
    public int amount = 1;
}
