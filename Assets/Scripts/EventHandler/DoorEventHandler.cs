﻿using System.Collections;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorEventHandler : MonoBehaviour
{
    public Item itemObtained;
    public RPGTalk rpgTalkHolder;

    public bool isLocked;
    public string destinationSceneName;

    public string lineStart;
    public string lineEnd;

    public bool isInFirstPersonMode;

    private bool playerIsInVicinityOfObject = false;
    private bool playerHasClickedObject = false;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerIsInVicinityOfObject = true;

            if (playerHasClickedObject)
            {
                beginItemInteraction();
                playerHasClickedObject = false;
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerIsInVicinityOfObject = false;
        }
    }


    void OnMouseDown()
    {
        if (!GameObject.FindGameObjectWithTag("Text Box") && !GameObject.FindGameObjectWithTag("Menu"))
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>().MoveTowards(gameObject.transform.position);
            playerHasClickedObject = true;
        }
    }

    private void beginItemInteraction()
    {
        if (!string.IsNullOrEmpty(lineStart) && !string.IsNullOrEmpty(lineEnd))
        {
            rpgTalkHolder.NewTalk(lineStart, lineEnd, rpgTalkHolder.txtToParse);
        }

        // If the door is unlocked but no destination scene was specified, that is a bug. This is reported.
        if (string.IsNullOrEmpty(destinationSceneName) && !isLocked)
        {
            Debug.LogError("Destination Scene is not specified! Please enter the destination scene in destinationSceneName field.");
        }
        else if (!isLocked)
        {
            StartCoroutine(LoadDestinationScene(destinationSceneName));
        }
    }

    private IEnumerator LoadDestinationScene(string sceneName)
    {
        yield return new WaitForSeconds(1);

        UnityEngine.AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

}
