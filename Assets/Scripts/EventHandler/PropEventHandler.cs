﻿using UnityEngine;
using UnityEngine.Events;

public class PropEventHandler : MonoBehaviour
{
    public Item itemObtained;
    public RPGTalk rpgTalkHolder;
    public GameObject interactionIndicator;

    private UnityEvent itemObtainedCallback;
    private UnityEvent itemObtainedDialogEndCallback;

    public string lineStart;
    public string lineEnd;

    public bool itemCanBeRepeatedlyObtained;
    public bool itemCanBeReached;

    public bool isInFirstPersonMode;

    private bool playerIsInVicinityOfObject = false;
    private bool playerHasClickedObject = false;

    void Start()
    {
        itemObtainedCallback = new UnityEvent();
        itemObtainedCallback.AddListener(obtainItem);

        itemObtainedDialogEndCallback = new UnityEvent();
        itemObtainedDialogEndCallback.AddListener(test);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerIsInVicinityOfObject = true;

            if (interactionIndicator != null)
            {
                interactionIndicator.SetActive(true);
            }

            if (playerHasClickedObject)
            {
                beginItemInteraction();
                playerHasClickedObject = false;
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerIsInVicinityOfObject = false;

            if (interactionIndicator != null)
            {
                interactionIndicator.SetActive(false);
            }
        }
    }


    void OnMouseDown()
    {
        if (isInFirstPersonMode)
        {
            beginItemInteraction();
        }
        else
        {
            if (playerIsInVicinityOfObject)
            {
                beginItemInteraction();
            }
            else if (!itemCanBeReached)
            {
                beginItemInteraction();
            }
            else
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>().MoveTowards(gameObject.transform.position);
                playerHasClickedObject = true;
            }
        }
    }

    private void beginItemInteraction()
    {
        if (itemObtained != null)
        {
            if (itemCanBeRepeatedlyObtained)
            {
                rpgTalkHolder.variables[0].variableValue = itemObtained.name;
            }
            else if (!itemCanBeRepeatedlyObtained)
            {
                Debug.Log("Player already has this item!");
            }
            else if (!itemCanBeRepeatedlyObtained)
            {
                rpgTalkHolder.variables[0].variableValue = itemObtained.name;
            }
        }

        rpgTalkHolder.NewTalk(lineStart, lineEnd, rpgTalkHolder.txtToParse, itemObtainedCallback);
    }

    public void obtainItem()
    {
        if (itemObtained != null)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>().AddItemInInventory(itemObtained);
            rpgTalkHolder.NewTalk("item_obtained_start", "item_obtained_end", rpgTalkHolder.txtToParse, itemObtainedDialogEndCallback);
        }
    }

    public void test()
    {
        
    }

}
